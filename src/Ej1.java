public class Ej1 {

    public static void main(String[] args) throws InterruptedException {
        Thread[] caballos = new Thread[10];
        for (int i = 0; i < 10; i++) {
            caballos[i] = new Thread(new ThreadHorse(i+1));
            caballos[i].start();
        }

        for (int i = 0; i < 10; i++) {
            caballos[i].join();
        }
    }

    public static class ThreadHorse implements Runnable{
            private int m = 5000;
            private int numero;

            ThreadHorse(int num){
                this.numero = num;
            }

            @Override
            public void run() {
                do {
                    for (int i = 1; i <= 10; i++) {
                        try {
                            m = m - 100;
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (m == 0) {
                            System.out.println("Caballo " + numero + " ha finalizado");
                        }else {
                            System.out.println("Caballo " + numero + ": " + m + "m para finalizar");
                        }
                    }
                }while(m > 0);
            }
    }

}
