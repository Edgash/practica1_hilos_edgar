public class Ej3 {

    //Al poner la prioridad máxima en el caballo 10 y la minima en el caballo 1, esta no es que funcione muy bien,
    //ya que aun con la prioridad hace lo que la CPU quiere, y no siempre da el resultado esperado

    public static void main(String[] args) throws InterruptedException {
        Thread[] caballos = new Thread[10];
        for (int i = 0; i < 10; i++) {
            caballos[i] = new Thread(new ThreadHorse(i + 1));
            if (i == 0) {
                caballos[i].setPriority(Thread.MIN_PRIORITY);
            } else if (i == 10){
                caballos[i].setPriority(Thread.MAX_PRIORITY);
            }
            caballos[i].start();
        }

        for (int i = 0; i < 10; i++) {
            caballos[i].join();
        }
    }

    public static class ThreadHorse implements Runnable{
        private int m = 5000;
        private int numero;

        ThreadHorse(int num){
            this.numero = num;
        }

        @Override
        public void run() {
            do {
                for (int i = 1; i <= 10; i++) {
                    try {
                        m = m - 100;
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (m == 0) {
                        System.out.println("Caballo " + numero + " ha finalizado");
                    }else {
                        System.out.println("Caballo " + numero + ": " + m + "m para finalizar");
                    }
                }
            }while(m>0);
        }
    }

}
