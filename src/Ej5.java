public class Ej5 {

    public static void main(String[] args) throws InterruptedException {

        Thread[] tortuga = new Thread[4];
        for (int i = 0; i < 4; i++) {
            tortuga[i] = new Thread(new animales.tortuga(i+1));
            tortuga[i].setPriority(Thread.MIN_PRIORITY);
            tortuga[i].start();

        }


        Thread[] conejo = new Thread[3];
        for (int i = 0; i < 3; i++) {
            conejo[i] = new Thread(new animales.conejo(i+1));
            tortuga[i].setPriority(Thread.NORM_PRIORITY);
            conejo[i].start();
        }


        Thread guepardo = new Thread(new animales.guepardo());
        guepardo.setPriority(Thread.MAX_PRIORITY);
        guepardo.start();

        for (int i = 0; i < 4; i++) {
            tortuga[i].join();
        }
        for (int i = 0; i < 3; i++) {
            conejo[i].join();
        }
        guepardo.join();
    }

    public static class animales {

        static class tortuga implements Runnable {
            private int m = 3000;
            private int numero;

            tortuga(int num){
                this.numero = num;
            }

            @Override
            public void run() {
                do {
                    for (int i = 1; i <= 4; i++) {
                        try {
                            m = m - 100;
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (m == 0) {
                            System.out.println("Tortuga " + numero + " ha finalizado");
                        }else {
                            System.out.println("Tortuga " + numero + ": " + m + "m para finalizar");
                        }
                    }
                }while(m > 0);
            }
        }

        static class conejo implements Runnable {
            private int m = 3000;
            private int numero;

            conejo(int num){
                this.numero = num;
            }

            @Override
            public void run() {
                do {
                    for (int i = 1; i <= 3; i++) {
                        try {
                            m = m - 100;
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (m == 0) {
                            System.out.println("Conejo " + numero + " ha finalizado");
                        }else {
                            System.out.println("Conejo " + numero + ": " + m + "m para finalizar");
                        }
                    }
                }while(m > 0);
            }
        }

        static class guepardo implements Runnable {
            private int m = 3000;
            @Override
            public void run() {
                do {
                    try {
                        m = m - 100;
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (m == 0) {
                        System.out.println("Guepardo ha finalizado");
                    } else {
                        System.out.println("Guepardo: " + m + "m para finalizar");
                    }
                } while (m > 0);
            }
        }
    }
}
