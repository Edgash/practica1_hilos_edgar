import java.util.Scanner;

public class Ej2 {
    public static void main(String[] args) throws InterruptedException {

        Scanner sc = new Scanner(System.in);
        System.out.println("Dame un número par");
        int num = sc.nextInt();

        if(num%2==0) {
            Thread pokemons = new Thread(new hilos(num));
            pokemons.start();
        }else{
            System.out.println("Numero impar");
        }

    }

    private static class hilos implements Runnable{
        private int num;

        public hilos(int num) {
            this.num = num;
        }

        @Override
        public void run() {
            for(int i = 1; i <= num; i++) {
                System.out.println("Soy el entrenador número " + i);
                System.out.println("Soy el pokémon número " + i + "\n");
            }
        }
    }
}